#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 10 10:41:07 2019

@author: delhaye
"""
import numpy as np
import math
from scipy.interpolate import griddata

def interpol_2d_G(matrix,lon_init,lat_init,lon_fin,lat_fin):

    # matrix, lon and lat must 2d.

    #Convert geographic coordinates to cartesian coordinates

    lon_init[lon_init>180] = lon_init[lon_init>180] - 360

    x_init = np.empty(shape=(lon_init.shape[0],lon_init.shape[1]))
    y_init = np.empty(shape=(lon_init.shape[0],lon_init.shape[1]))
    for x_i in np.arange(lon_init.shape[0]):
        for y_i in np.arange(lon_init.shape[1]):
            x_init[x_i,y_i] = 110.949*(-90 - lat_init[x_i,y_i])*math.cos(math.radians(lon_init[x_i,y_i]))
            y_init[x_i,y_i] = 110.949*(-90 - lat_init[x_i,y_i])*math.sin(math.radians(lon_init[x_i,y_i]))

    x_final = np.empty(shape=(lon_fin.shape[0],lon_fin.shape[1]))
    y_final = np.empty(shape=(lon_fin.shape[0],lon_fin.shape[1]))
    for x_i in np.arange(lon_fin.shape[0]):
        for y_i in np.arange(lon_fin.shape[1]):
            x_final[x_i,y_i] = 110.949*(-90 - lat_fin[x_i,y_i])*math.cos(math.radians(lon_fin[x_i,y_i]))
            y_final[x_i,y_i] = 110.949*(-90 - lat_fin[x_i,y_i])*math.sin(math.radians(lon_fin[x_i,y_i]))

    LON_LAT_init = np.reshape(x_init, (x_init.size,1))
    LON_LAT_init = np.concatenate((LON_LAT_init,np.reshape(y_init, (y_init.size,1))),axis=1)

    LON_LAT_final = np.reshape(x_final, (x_final.size,1))
    LON_LAT_final = np.concatenate((LON_LAT_final,np.reshape(y_final, (y_final.size,1))),axis=1)

    data = np.reshape(matrix,(np.size(matrix),1))

    b = griddata(LON_LAT_init,data,LON_LAT_final,method='linear')
    b_rshp = np.reshape(b,(lon_fin.shape[0],lon_fin.shape[1]))

    return b_rshp