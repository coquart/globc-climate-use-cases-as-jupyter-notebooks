#!/bin/bash

t="Amon"
vari=("tas" "psl" "ua" "pr")
for var in "${vari[@]}"; do

    for i in {1..100..1}; do
       scp /data/scratch/globc/dcom/CMIP6/PAMIP/CNRM-CERFACS/CNRM-CM6-1/pdSST-futArcSIC/CNRM-CM6-1_pdSST-futArcSIC_r${i}i1p1f2/${var}_${t}_CNRM-CM6-1_pdSST-futArcSIC_r${i}i1p1f2_gr_200004-200105.nc delhaye@lorenz:/afast/delhaye/wget/PAMIP/pdSST-futArcSIC/${var}/CNRM-CM6-1/
       #scp /data/scratch/globc/dcom/CMIP6/PAMIP/CNRM-CERFACS/CNRM-CM6-1/pdSST-pdSIC/CNRM-CM6-1_pdSST-pdSIC_r${i}i1p1f2/${var}_${t}_CNRM-CM6-1_pdSST-pdSIC_r${i}i1p1f2_gr_200004-200105.nc delhaye@lorenz:/afast/delhaye/wget/PAMIP/pdSST-pdSIC/${var}/CNRM-CM6-1/
    done
done
