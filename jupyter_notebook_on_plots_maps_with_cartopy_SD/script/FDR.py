#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  7 16:16:59 2020

@author: delhaye
"""

import numpy as np
import statsmodels
from scipy import stats
import statsmodels.api as sm

def fdr_correction(p_val):
    nx,ny = p_val.shape
    #From 2D to 1D
    all_pval = p_val.ravel() 
    
    #Compute the p-val 
    fdr = statsmodels.stats.multitest.multipletests(all_pval, alpha=0.05, method='fdr_bh', is_sorted=False, returnsorted=False)[1]
    
    #p-val corrected in the same shape than p_val
    fdr_pval = fdr.reshape(nx,ny)
    
    return(fdr_pval)
