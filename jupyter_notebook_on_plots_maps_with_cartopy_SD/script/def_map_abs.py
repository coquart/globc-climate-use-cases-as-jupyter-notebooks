#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 16:04:29 2020

@author: delhaye
"""

#Standard libraries
from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
import matplotlib; matplotlib.use('Agg')
#from mpl_toolkits.basemap import Basemap, maskoceans
#from mpl_toolkits import basemap as bm
#from mpl_toolkits.basemap import shiftgrid
from scipy.stats import norm
from scipy import stats
#from sklearn.utils import resample

#from FDR import fdr_correction

import scipy.io as sio
import math
from scipy.interpolate import griddata

from def_interpol_2d import interpol_2d_G

#####
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker

def map_month_abs(var,model,level,ctrl,pert,lon,lat,dir_fig,region):
    if var == "tas" :
    #        scale = [-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6]
        scale = np.round(np.arange(-2,2.1,0.1),1)
    elif var == "hfss" or var == "hfls" or var =="hf":
        scale = [-20,-17.5,-15,-12.5,-10,-7.5,-5,-2.5,0,2.5,5,7.5,10,12.5,15,17.5,20]
    elif var == "tos":
        scale = [-2,-1.75,-1.5,-1.25,-1,-0.75,-0.5,-0.25,0,0.25,0.5,0.75,1,1.25,1.5,1.75,2]
    elif var =="P-E":
        scale = [-25,-22.5,-20,-17.5,-15,-12.5,-10,-7.5,-5,-2.5,0,2.5,5,7.5,10,12.5,15,17.5,20,22.5,25]
    elif var =="rsus":
        scale = [-20,-17.5,-15,-12.5,-10,-7.5,-5,-2.5,0,2.5,5,7.5,10,12.5,15,17.5,20]
    elif var =="rlut":
        scale = [-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6]
    elif var =="mrso":
        scale = np.arange(-10,11,2)
    elif var =="pr":
        scale = [-20,-17.5,-15,-12.5,-10,-7.5,-5,-2.5,0,2.5,5,7.5,10,12.5,15,17.5,20]
    elif var =="prsn":
        scale = [-15,-12.5,-10,-7.5,-5,-2.5,0,2.5,5,7.5,10,12.5,15]
    elif var =="prc":
        scale = [-20,-17.5,-15,-12.5,-10,-7.5,-5,-2.5,0,2.5,5,7.5,10,12.5,15,17.5,20]
    elif var =="snw":
        scale=[-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10]
    elif var == "zg"or var == "psl":
        scale = [-2,-1.75,-1.5,-1.25,-1,-0.75,-0.5,-0.25,0,0.25,0.5,0.75,1,1.25,1.5,1.75,2]
        if var =="psl":
            scale_contour = np.arange(1000,1024.1,4)
    elif var =="ua" or var =="va":
        scale = [-2,-1.75,-1.5,-1.25,-1,-0.75,-0.5,-0.25,0,0.25,0.5,0.75,1,1.25,1.5,1.75,2]
        if var=="ua":
            scale_contour = np.arange(0,48.1,8)
        else:
            scale_contour = np.arange(-12,12.1,3)
    elif var =="wap":
        scale=[-1.4,-1.2,-1,-0.8,-0.6,-0.4,-0.2,0,0.2,0.4,0.6,0.8,1,1.2,1.4]
#        scale_contour = np.arange(-12,12.1,3)
    elif var =="siconc":
        scale = np.arange(-60,1,5)
    else:
        scale = [-20,-15,-10,-5,0,5,10,15,20]

    # Palette
    if var =="pr" or var =="P-E" or var=="mrso" or var =="prc" or var =="prsn":
        palette_t2m=plt.cm.RdBu
    elif var == "tas" or var =="zg" or var=="psl":
        palette_t2m=plt.cm.seismic
    elif var =="siconc":
        palette_t2m=plt.cm.Reds_r
    else:
        palette_t2m=plt.cm.RdBu_r

    lon[0] = 0
    lon[-1] = 360
    nx = ctrl[0,0,:,0].size
    ny = ctrl[0,0,0,:].size

    if region==0:
        map = Basemap(projection="npaeqd", boundinglat=30,lon_0=0,resolution='l')
        lon_s = lon
    elif region ==1:
        # Map projection
        lon1 = -40.
        lon2 = 60.
        lat1 = 30.
        lat2 = 72.

        #Carte avec une proj et de la lat1 �|  la lat2 et de la long1 �|  la long 2
        map = Basemap(projection='merc',llcrnrlat=lat1,urcrnrlat=lat2,llcrnrlon=lon1,urcrnrlon=lon2,resolution='c')
    elif region==2:

        map = Basemap(projection='merc',llcrnrlat=20,urcrnrlat=75,\
        llcrnrlon=-180,urcrnrlon=180,lat_ts=20,resolution='c')

    elif region == 3:

        map = Basemap(projection='moll',lon_0=0,resolution='c')



    ########Pour interpoller sur grille régulière
    data_PLOT=sio.loadmat('/data/home/globc/delhaye/script/LAT_eq_PN_51.mat')
    LAT_RACMO = data_PLOT["LAT_eq"]
    data_lon=sio.loadmat('/data/home/globc/delhaye/script/LON_eq_PN_51.mat')
    LON_RACMO = data_lon["LON_eq"]
    if region ==1 or region==2 or region==3:
        for i in np.arange(LON_RACMO[:,0].size):
            for j in np.arange(LON_RACMO[:,0].size):
                if LON_RACMO[i,j]>180:
                    LON_RACMO[i,j]=LON_RACMO[i,j]-360


    x1,y1 = map(LON_RACMO,LAT_RACMO)


    for i in np.arange(15):
        fig,ax=plt.subplots()


        ####Calcul p-val selon test de Kolmogorov 2 sample
        p_val = np.zeros((nx,ny))
        for n in np.arange(nx):
            for m in np.arange(ny):
#                p_val[n,m] = stats.ks_2samp(ctrl[:,i,n,m], pert[:,i,n,m])[1] 
                p_val[n,m] = stats.ks_2samp(ctrl[:,i,n,m], pert[:,i,n,m])[1]
#        p_val_fdr = fdr_correction(p_val)
        if region==0:
            lon_s = lon
            ctrlu = np.mean(ctrl,axis=0)
            pertu = np.mean(pert,axis=0)
        else:
            p_val, lon_s = shiftgrid(180.,p_val, lon, start=False)
           # p_val_fdr, lon_s = shiftgrid(180.,p_val_fdr, lon, start=False)
            ctrlu, lon_s = shiftgrid(180., ctrl, lon, start=False)
            pertu,lon_s = shiftgrid(180., pert, lon, start=False)
        if var =="siconc":
            lon_mesh,lat_mesh = lon,lat
        else:
            lon_mesh,lat_mesh = np.meshgrid(lon_s,lat)
        x,y = map(lon_mesh,lat_mesh)
        cs = map.contourf(x,y,pertu[i,:,:]-ctrlu[i,:,:],scale,cmap=palette_t2m,extend="both")
        grid_stat = interpol_2d_G(p_val,lon_mesh,lat_mesh,LON_RACMO,LAT_RACMO)
        #grid_stat_fdr = interpol_2d_G(p_val_fdr,lon_mesh,lat_mesh,LON_RACMO,LAT_RACMO)
        if i == 0:
            name = "01_Jan"
        elif i ==1:
            name = "02_Feb"
        elif i ==2:
            name = "03_Mar"
        elif i ==3:
            name = "04_Apr"
        elif i ==4:
            name = "05_May"
        elif i ==5:
            name = "06_Jun"
        elif i ==6:
            name = "07_Jul"
        elif i ==7:
            name = "08_Aug"
        elif i ==8:
            name = "09_Sep"
        elif i ==9:
            name = "10_Oct"
        elif i ==10:
            name = "11_Nov"
        elif i ==11:
            name = "12_Dec"
        elif i ==12:
            name = "13_Jan"
        elif i == 13:
            name = "14_Feb"
        elif i ==14:
            name = "15_Mar"

        if var =="hfls" or var =="hfss" or var == "hf"or var =="tos" or var =="rsus":
            map.fillcontinents()
        elif var =="mrso":
            map.drawlsmask(ocean_color='blue')
        map.drawparallels(np.arange(-80,81,20.),labels=[1,0,0,0],color='black',linewidth=0.2)
        map.drawmeridians(np.arange(-180,181.,60.),labels=[0,0,0,1],color='black',linewidth=0.2)
        map.drawcountries()
        map.drawcoastlines()
        if region ==0:
            if var =="rlut":
                print('no scale')
            else:
                map.drawmapscale(-45,17,0,0,1000)
        elif region==1:
            map.drawmapscale(-25,35,0,0,1000)
        #échelle mise en bas
        if region==2 or region==3:
            cbar = map.colorbar(cs,location='bottom',pad="20%")
        else:
            cbar = map.colorbar(cs,location='bottom',pad="5%")

        #Nom échelle
        if var == "pr":
            cbar.set_label('Precipitation $_{PERT-CTRL}$ (mm)',fontsize=16)
        elif var =="tas":
            cbar.set_label('Surface Temperature \n $_{Low-High}$ ($^\circ$C)',fontsize=16)
        elif var == "psl":
            cbar.set_label('Sea Level Pressure $_{PERT-CTRL}$ (hPa)',fontsize=16)
        elif var =="hfss":
            cbar.set_label('Surface Sensible Heat Flux \n $_{PERT-CTRL}$ (W/m²)',fontsize=16)
        elif var =="hfls":
            cbar.set_label('Surface Latent Heat Flux \n $_{PERT-CTRL}$ (W/m²)',fontsize=16)
        elif var =="hf":
            cbar.set_label('Surface Turbulent Heat Flux \n $_{PERT-CTRL}$ (W/m²)',fontsize=16)
        elif var =="tos":
            cbar.set_label('SST $_{PERT-CTRL}$ ($^\circ$C)',fontsize=16)
        elif var =="P-E":
             cbar.set_label('P-E $_{PERT-CTRL}$ (mm) ',fontsize=16)
        elif var == "evspsbl":
            cbar.set_label('Evaporation $_{PERT-CTRL}$ (mm)',fontsize=16)
        elif var == "rsus":
            cbar.set_label('Surface Upwelling Shortwave \n Radiation $_{PERT-CTRL}$ (W/m²)',fontsize=16)
        elif var == "rlut":
            cbar.set_label('TOA OLR $_{PERT-CTRL}$ (W/m²)',fontsize=16)
        elif var =="mrso":
            cbar.set_label('Soil Moisture Content $_{PERT-CTRL}$ (kg/m²)',fontsize=16)
        elif var =="prsn":
            cbar.set_label('Snow Precipitation $_{PERT-CTRL}$ (mm)',fontsize=16)
        elif var =="prc":
            cbar.set_label('Convective Precipitation $_{PERT-CTRL}$ (mm)',fontsize=16)
        elif var =="snw":
            cbar.set_label('Snow Depth $_{PERT-CTRL}$ (mm)',fontsize=16)
        elif var == "ua":
            cbar.set_label('Zonal Wind at '+str(level)+'hPa $_{PERT-CTRL}$ (m/s)',fontsize=16)
        elif var == "va":
            cbar.set_label('Meridional Wind at '+str(level)+'hPa $_{PERT-CTRL}$ (m/s)',fontsize=16)
        elif var =="ta":
            cbar.set_label('Temperature at '+str(level)+'hPa \n $_{PERT-CTRL}$ ($^\circ$C)',fontsize=16)
        elif var == "zg":
            cbar.set_label('Geopotential Height at '+str(level)+'hPa \n $_{PERT-CTRL} (dam)$',fontsize=16)
        elif var =="wap":
            cbar.set_label('Vertical velocity at '+str(level)+'hPa \n $_{PERT-CTRL} (hPa/s)$',fontsize=16)
        elif var=="siconc":
            cbar.set_label('Sea Ice Concentration $_{PERT-CTRL}$ (%)',fontsize=14)
        cbar.ax.tick_params(labelsize=14)
        cbar.set_ticks(scale[::5])
        cbar.set_ticklabels(scale[::5])

        sign = 0.05
        cs = map.scatter(x1,y1,s=grid_stat<sign,marker='1',color='k')
#        cs = map.scatter(x1,y1,s=grid_stat_fdr<sign,marker='o',color='k')

        if region == 0:
            figname = 'Diff_'+var+'_'+str(sign)+'_'+name+'_'+'.png'
        elif region ==1:
            figname = 'Europe_Diff_'+var+'_'+str(sign)+'_'+name+'_'+'.png'
        elif region==2:
            figname= 'Midlat_Diff_'+var+'_'+str(sign)+'_'+name+'_'+'.png'
        elif region ==3:
            figname= 'World_Diff_'+var+'_'+str(sign)+'_'+name+'_'+'.png'

        plt.title(model+"  Nl="+str(pert[:,0,0,0].size)+"  Nh="+str(ctrl[:,0,0,0].size)+ "  " +name[3:6])
        fig.savefig(dir_fig + figname,dpi=200,bbox_inches='tight')


def map_season_abs_all(var,model,level,ctrl,pert,lon,lat,dir_fig,region):

    if var == "tas" :
    #        scale = [-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6]
        scale = np.round(np.arange(-2,2.1,0.1),1)
    elif var == "hfss" or var == "hfls" or var =="hf":
        scale = [-20,-17.5,-15,-12.5,-10,-7.5,-5,-2.5,0,2.5,5,7.5,10,12.5,15,17.5,20]
    elif var == "tos":
        scale = [-2,-1.75,-1.5,-1.25,-1,-0.75,-0.5,-0.25,0,0.25,0.5,0.75,1,1.25,1.5,1.75,2]
    elif var =="P-E":
        scale = [-25,-22.5,-20,-17.5,-15,-12.5,-10,-7.5,-5,-2.5,0,2.5,5,7.5,10,12.5,15,17.5,20,22.5,25]
    elif var =="rsus":
        scale = [-20,-17.5,-15,-12.5,-10,-7.5,-5,-2.5,0,2.5,5,7.5,10,12.5,15,17.5,20]
    elif var =="rlut":
        scale = [-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6]
    elif var =="mrso":
        scale = np.arange(-10,11,2)
    elif var =="pr":
        scale = [-20,-17.5,-15,-12.5,-10,-7.5,-5,-2.5,0,2.5,5,7.5,10,12.5,15,17.5,20]
    elif var =="prsn":
        scale = [-15,-12.5,-10,-7.5,-5,-2.5,0,2.5,5,7.5,10,12.5,15]
    elif var =="prc":
        scale = [-20,-17.5,-15,-12.5,-10,-7.5,-5,-2.5,0,2.5,5,7.5,10,12.5,15,17.5,20]
    elif var =="snw":
        scale=[-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10]
    elif var == "zg"or var == "psl":
        scale = [-2,-1.75,-1.5,-1.25,-1,-0.75,-0.5,-0.25,0,0.25,0.5,0.75,1,1.25,1.5,1.75,2]
        if var =="psl":
            scale_contour = np.arange(1000,1024.1,4)
    elif var =="ua" or var =="va":
        scale = [-2,-1.75,-1.5,-1.25,-1,-0.75,-0.5,-0.25,0,0.25,0.5,0.75,1,1.25,1.5,1.75,2]
        if var=="ua":
            scale_contour = np.arange(0,48.1,8)
        else:
            scale_contour = np.arange(-12,12.1,3)
    elif var =="wap":
        scale=[-1.4,-1.2,-1,-0.8,-0.6,-0.4,-0.2,0,0.2,0.4,0.6,0.8,1,1.2,1.4]
#        scale_contour = np.arange(-12,12.1,3)
    elif var =="siconc":
        scale = np.arange(-60,1,5)
    else:
        scale = [-20,-15,-10,-5,0,5,10,15,20]

    # Palette
    if var =="pr" or var =="P-E" or var=="mrso" or var =="prc" or var =="prsn":
        palette_t2m=plt.cm.RdBu
    elif var == "tas" or var =="zg" or var=="psl":
        palette_t2m=plt.cm.seismic
    elif var =="siconc":
        palette_t2m=plt.cm.Reds_r
    else:
        palette_t2m=plt.cm.RdBu_r

    lon[0] = 0
    lon[-1] = 360
    nx = ctrl[0,0,:,0].size
    ny = ctrl[0,0,0,:].size

    if region==0:
        map = Basemap(projection="npaeqd", boundinglat=30,lon_0=0,resolution='l')
        lon_s = lon
    elif region ==1:
        # Map projection
        lon1 = -40.
        lon2 = 60.
        lat1 = 30.
        lat2 = 72.

        #Carte avec une proj et de la lat1 �|  la lat2 et de la long1 �|  la long 2
        map = Basemap(projection='merc',llcrnrlat=lat1,urcrnrlat=lat2,llcrnrlon=lon1,urcrnrlon=lon2,resolution='c')
    elif region==2:

        map = Basemap(projection='merc',llcrnrlat=20,urcrnrlat=75,\
        llcrnrlon=-180,urcrnrlon=180,lat_ts=20,resolution='c')

    elif region == 3:

        map = Basemap(projection='moll',lon_0=0,resolution='c')



    ########Pour interpoller sur grille régulière
    data_PLOT=sio.loadmat('/data/home/globc/delhaye/script/LAT_eq_PN_51.mat')
    LAT_RACMO = data_PLOT["LAT_eq"]
    data_lon=sio.loadmat('/data/home/globc/delhaye/script/LON_eq_PN_51.mat')
    LON_RACMO = data_lon["LON_eq"]

    if region ==1 or region==2 or region==3:
        for i in np.arange(LON_RACMO[:,0].size):
            for j in np.arange(LON_RACMO[:,0].size):
                if LON_RACMO[i,j]>180:
                    LON_RACMO[i,j]=LON_RACMO[i,j]-360


    x1,y1 = map(LON_RACMO,LAT_RACMO)

    if region==0:
        lon_s = lon
        ctrlu = np.mean(ctrl,axis=0)
        pertu = np.mean(pert,axis=0)
    else:
        ctrlu = np.mean(ctrl,axis=0)
        pertu= np.mean(pert,axis=0)
        pertu,lon_s = shiftgrid(180., pertu, lon, start=False)
        ctrlu, lon_s = shiftgrid(180., ctrlu, lon, start=False)

    j = 3
    for i in np.arange(4):
        fig,ax=plt.subplots()


        ####Calcul p-val selon test de Kolmogorov 2 sample
        p_val = np.zeros((nx,ny))
        for n in np.arange(nx):
            for m in np.arange(ny):
                p_val[n,m] = stats.ks_2samp(np.mean(ctrl[:,j:j+3,n,m],axis=1), np.mean(pert[:,j:j+3,n,m],axis=1))[1]
        p_val_fdr = fdr_correction(p_val)


        if region != 0:
            p_val, lon_s = shiftgrid(180.,p_val, lon, start=False) 
#            p_val_fdr, lon_s = shiftgrid(180.,p_val_fdr, lon, start=False)
        if var =="siconc":
            lon_mesh,lat_mesh = lon,lat
        else:
            lon_mesh,lat_mesh = np.meshgrid(lon_s,lat)
        x,y = map(lon_mesh,lat_mesh)
        cs = map.contourf(x,y,np.mean(pertu[j:j+3,:,:]-ctrlu[j:j+3,:,:], axis=0),scale,cmap=palette_t2m,extend="both")
#        grid_stat_fdr = interpol_2d_G(p_val_fdr,lon_mesh,lat_mesh,LON_RACMO,LAT_RACMO)
        grid_stat = interpol_2d_G(p_val,lon_mesh,lat_mesh,LON_RACMO,LAT_RACMO)
        if i ==0:
            name = "0_AMJ_yr1_abs"
        elif i == 1:
            name = "1_JAS_yr1_abs"
        elif i ==2:
            name = "2_OND_yr1_abs"
        elif i ==3:
            name = "3_JFM_yr2_abs"

        if var =="hfls" or var =="hfss" or var == "hf"or var =="tos" or var =="rsus":
            map.fillcontinents()
        elif var =="mrso":
            map.drawlsmask(ocean_color='blue')
        map.drawparallels(np.arange(-80,81,20.),labels=[1,0,0,0],color='black',linewidth=0.2)
        map.drawmeridians(np.arange(-180,181.,60.),labels=[0,0,0,1],color='black',linewidth=0.2)
        map.drawcountries()
        map.drawcoastlines()
        if region ==0:
            if var =="rlut":
                print('no scale')
            else:
                map.drawmapscale(-45,17,0,0,1000)
        elif region==1:
            map.drawmapscale(-25,35,0,0,1000)

        #échelle mise en bas
        if region==2 or region==3:
            cbar = map.colorbar(cs,location='bottom',pad="20%")
        else:
            cbar = map.colorbar(cs,location='bottom',pad="5%")

        #Nom échelle
        if var == "pr":
            cbar.set_label('Precipitation $_{PERT-CTRL}$ (mm)',fontsize=16)
        elif var =="tas":
            cbar.set_label('Surface Temperature \n $_{Low-High}$ ($^\circ$C)',fontsize=16)
        elif var == "psl":
            cbar.set_label('Sea Level Pressure $_{PERT-CTRL}$ (hPa)',fontsize=16)
        elif var =="hfss":
            cbar.set_label('Surface Sensible Heat Flux \n $_{PERT-CTRL}$ (W/m²)',fontsize=16)
        elif var =="hfls":
            cbar.set_label('Surface Latent Heat Flux \n $_{PERT-CTRL}$ (W/m²)',fontsize=16)
        elif var =="hf":
            cbar.set_label('Surface Turbulent Heat Flux \n $_{PERT-CTRL}$ (W/m²)',fontsize=16)
        elif var =="tos":
            cbar.set_label('SST $_{PERT-CTRL}$ ($^\circ$C)',fontsize=16)
        elif var =="P-E":
             cbar.set_label('P-E $_{PERT-CTRL}$ (mm) ',fontsize=16)
        elif var == "evspsbl":
            cbar.set_label('Evaporation $_{PERT-CTRL}$ (mm)',fontsize=16)
        elif var == "rsus":
            cbar.set_label('Surface Upwelling Shortwave \n Radiation $_{PERT-CTRL}$ (W/m²)',fontsize=16)
        elif var == "rlut":
            cbar.set_label('TOA OLR $_{PERT-CTRL}$ (W/m²)',fontsize=16)
        elif var =="mrso":
            cbar.set_label('Soil Moisture Content $_{PERT-CTRL}$ (kg/m²)',fontsize=16)
        elif var =="prsn":
            cbar.set_label('Snow Precipitation $_{PERT-CTRL}$ (mm)',fontsize=16)
        elif var =="prc":
            cbar.set_label('Convective Precipitation $_{PERT-CTRL}$ (mm)',fontsize=16)
        elif var =="snw":
            cbar.set_label('Snow Depth $_{PERT-CTRL}$ (mm)',fontsize=16)
        elif var == "ua":
            cbar.set_label('Zonal Wind at '+str(level)+'hPa $_{PERT-CTRL}$ (m/s)',fontsize=16)
        elif var == "va":
            cbar.set_label('Meridional Wind at '+str(level)+'hPa $_{PERT-CTRL}$ (m/s)',fontsize=16)
        elif var =="ta":
            cbar.set_label('Temperature at '+str(level)+'hPa \n $_{PERT-CTRL}$ ($^\circ$C)',fontsize=16)
        elif var == "zg":
            cbar.set_label('Geopotential Height at '+str(level)+'hPa \n $_{PERT-CTRL} (dam)$',fontsize=16)
        elif var =="wap":
            cbar.set_label('Vertical velocity at '+str(level)+'hPa \n $_{PERT-CTRL} (hPa/s)$',fontsize=16)
        elif var=="siconc":
            cbar.set_label('Sea Ice Concentration $_{PERT-CTRL}$ (%)',fontsize=14)
        cbar.ax.tick_params(labelsize=14)
        cbar.set_ticks(scale[::5])
        cbar.set_ticklabels(scale[::5])
        plt.title(model+"  Nl="+str(pert[:,0,0,0].size)+"  Nh="+str(ctrl[:,0,0,0].size)+ "  " +name[2:5])
        sign = 0.05
        cs = map.scatter(x1,y1,s=grid_stat<sign,marker='1',color='k')
#        cs = map.scatter(x1,y1,s=grid_stat_fdr<sign,marker='o',color='k')

        for label in cbar.ax.xaxis.get_ticklabels()[1::2]:
            label.set_visible(False)
        if region == 0 or region==1:
            figname = 'Diff_'+var+'_'+str(sign)+'_'+name+'.png'
        elif region==2:
            figname= 'Midlat_Diff_'+var+'_'+str(sign)+'_'+name+'.png'
        elif region ==3:
            figname= 'World_Diff_'+var+'_'+str(sign)+'_'+name+'.png'

        fig.savefig(dir_fig + figname,dpi=200,bbox_inches='tight')

        j = j +3


