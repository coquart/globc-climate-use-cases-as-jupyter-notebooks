#!/usr/bin/env python3
## -*- coding: utf-8 -*-
#"""
#Created on Mon Feb  3 16:04:29 2020
#
#@author: delhaye
#"""
#


# Standard libraries
import numpy as np
import matplotlib.pyplot as plt
import pickle
import os, subprocess

from netCDF4 import Dataset
#from mpl_toolkits.basemap import Basemap
from scipy.stats import norm
from scipy import stats
from def_map_abs import map_season_abs_all

#############
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker


fig,ax = plt.subplots(figsize=(12,6))

dir_ctrl ='/afast/delhaye/wget/siconc/piControl/remap/pickle/'
dir_fig = '/elic/home/delhaye/CMIP6/piControl/tas/'
dir_tas ='/data/scratch/globc/dcom/CMIP6/CMIP/CNRM-CERFACS/CNRM-CM6-1/piControl/r1i1p1f2/Amon/tas/gr/latest/'

fig,ax = plt.subplots(figsize=(12,6))
i = 1
num = 1


c = open(dir_ctrl + files,'rb')
extent1 = pickle.load(c)
extent_ctrl = extent1[0]
nc_file = dir_tas + 'tas_Amon_'+ files[13:-2] + ".nc"
fh = Dataset(nc_file, mode='r')
   
lon = fh.variables['lon'][:]
lat = fh.variables['lat'][:]
   
tas = fh.variables["tas"][:]
tas = tas - 273.15
fh.close()
   

map_season_abs_all("tas",model,0,c,p,lon,lat,dir_fig1,0)





#ax.legend(loc='upper right',shadow=True,frameon=False,fontsize=13,ncol=3)
#plt.xlabel('Years',fontsize=18)
#plt.ylabel('Sept Arctic Sea Ice Extent (10$^6$ km$^2$)',fontsize=18)
#plt.yticks(np.arange(0, 24.1, 2),fontsize=18)
#plt.title('piControl',fontsize=26)
#plt.grid()
#fig.savefig(dir_fig+ "SI_extent_sep_all_CTRL.png",dpi=200,bbox_inches='tight')

#ax.legend(loc='upper right',shadow=True,frameon=False,fontsize=13,ncol=3)
#plt.xlabel('Years',fontsize=18)
#plt.ylabel('Mars Arctic Sea Ice Extent (10$^6$ km$^2$)',fontsize=18)
#plt.yticks(np.arange(0, 24.1, 2),fontsize=18)
#plt.title('piControl',fontsize=26)
#plt.grid()
#fig.savefig(dir_fig+ "SI_extent_mar_all_CTRL.png",dpi=200,bbox_inches='tight')



