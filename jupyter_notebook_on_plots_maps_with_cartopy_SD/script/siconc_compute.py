#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 18 09:58:13 2018

@author: delhaye
"""

#Importe Dataset de netCDF4
from netCDF4 import Dataset
#Importe numpy et le nomme np dans le script
#numpy permet de travailler avce des tableaux et matrices
import numpy as np
import numpy.ma as ma
import pickle
import os, subprocess

dir_ctrl ='/afast/delhaye/wget/siconc/piControl/remap/'
dir_grid = '/afast/delhaye/mask/'

# Function to compute Arctic sea ice extent - NEMO
def compute_extent(nm,lat,lat_threshold,siconc,siconc_threshold,area,tmask):
    extent = np.zeros(nm)

    for i in np.arange(nm):
        indices = (lat >= lat_threshold) * (siconc[i,:,:] >= siconc_threshold) * (siconc[i,:,:] <= 100)* (tmask[0,0,:,:] == 1)
        extent[i] = (area[0,:,:]*[indices]).sum()
    extent = extent / 10**6.


    return extent

def compute_extent_ATL(nm,lat,lat_threshold,siconc,siconc_threshold,area,tmask):
    extent = np.zeros(nm)

    for i in np.arange(nm):
        indices = (lat >= 70) * (lat <= 82) * (lon>15) * (lon<100)* (siconc[i,:,:] >= siconc_threshold) * (siconc[i,:,:] <= 100)* (tmask[0,0,:,:] == 1)
        extent[i] = (area[0,:,:]*[indices]).sum()
    extent = extent / 10**6.


    return extent

def compute_extent_PAC(nm,lat,lat_threshold,siconc,siconc_threshold,area,tmask):
    extent = np.zeros(nm)

    for i in np.arange(nm):
        indices = (lat >= 50) * (lat <= 82) * (lon>170) * (lon<200)* (siconc[i,:,:] >= siconc_threshold) * (siconc[i,:,:] <= 100)* (tmask[0,0,:,:] == 1)
        extent[i] = (area[0,:,:]*[indices]).sum()
    extent = extent / 10**6.


    return extent

def compute_extent_both(nm,lat,lat_threshold,siconc,siconc_threshold,area,tmask):
    extent_PAC = np.zeros(nm)
    extent_ATL = np.zeros(nm)
    for i in np.arange(nm):
        indices_PAC = (lat >= 50) * (lat <= 82) * (lon>170) * (lon<200)* (siconc[i,:,:] >= siconc_threshold) * (siconc[i,:,:] <= 100)* (tmask[0,0,:,:] == 1)
        extent_PAC[i] = (area[0,:,:]*[indices_PAC]).sum()
        indices_ATL = (lat >= 70) * (lat <= 82) * (lon>15) * (lon<100)* (siconc[i,:,:] >= siconc_threshold) * (siconc[i,:,:] <= 100)* (tmask[0,0,:,:] == 1)
        extent_ATL[i] = (area[0,:,:]*[indices_ATL]).sum()

    extent = (extent_PAC / 10**6.)+(extent_ATL / 10**6.)


    return extent

def compute_extent_GRATL(nm,lat,lat_threshold,siconc,siconc_threshold,area,tmask):
    extent_ATL = np.zeros(nm)
    extent_GR1 = np.zeros(nm)
    extent_GR2 = np.zeros(nm)
    for i in np.arange(nm):
        indices = (lat >= 70) * (lat <= 82) * (lon>15) * (lon<100)* (siconc[i,:,:] >= siconc_threshold) * (siconc[i,:,:] <= 100)* (tmask[0,0,:,:] == 1)
        extent_ATL[i] = (area[0,:,:]*[indices]).sum()
        indices_GR1 = (lat >= 70) * (lat <= 82) * (lon>320) * (siconc[i,:,:] >= siconc_threshold) * (siconc[i,:,:] <= 100)* (tmask[0,0,:,:] == 1)
        extent_GR1[i] = (area[0,:,:]*[indices_GR1]).sum()
        indices_GR2 = (lat >= 70) * (lat <= 82) * (lon<15) * (siconc[i,:,:] >= siconc_threshold) * (siconc[i,:,:] <= 100)* (tmask[0,0,:,:] == 1)
        extent_GR2[i] = (area[0,:,:]*[indices_GR2]).sum()
    extent_tot = (extent_ATL / 10**6.)+(extent_GR1 / 10**6.)+(extent_GR2 / 10**6.)


    return extent_tot



# Thresholds
lat_threshold = 0. # latitude (degrees north)
siconc_threshold = 15.0 # sea ice concentration
nc_file = dir_grid + 'ORCA1_mesh_mask.nc'
fh = Dataset(nc_file, mode='r')
e1 = fh.variables['e1t'][:]
e2 = fh.variables['e2t'][:]
area = (e1 * e2) / 1.e6
tmask = fh.variables['tmask'][:]
fh.close()

for files in os.listdir(dir_ctrl):
    print(files)
    
    nc_file = dir_ctrl + files
    fh = Dataset(nc_file, mode='r')
    lat = fh.variables['latitude'][:]
    siconc = fh.variables['siconc'][:]
    lon = fh.variables['longitude'][:]
    nm,nx,ny = siconc.shape
    fh.close() 
    
    siconc = siconc.filled()
    
    extent_GRATL = compute_extent_GRATL(nm,lat,lat_threshold,siconc,siconc_threshold,area,tmask)
    f = open(dir_ctrl + 'pickle/GRATL/GRATL_'+files[:-3] +'.p','wb')
    pickle.dump([extent_GRATL],f)
    f.close()
    print(extent_GRATL[:12])

   # extent = compute_extent(nm,lat,lat_threshold,siconc,siconc_threshold,area,tmask)

   # f = open(dir_ctrl + 'pickle/'+files[:-3] +'.p','wb')
   # #Enregistre chaque extent dans un fichier "extent-1990-1999.p"
   # pickle.dump([extent],f)
   # f.close()
   # 
   # extent_ATL = compute_extent_ATL(nm,lat,lat_threshold,siconc,siconc_threshold,area,tmask)
   # print(extent_ATL[:12])
   # 
   # f = open(dir_ctrl + 'pickle/ATL/ATL_'+files[:-3] +'.p','wb')
   # #Enregistre chaque extent dans un fichier "extent-1990-1999.p"
   # pickle.dump([extent_ATL],f)
   # f.close()
   # 
   # 
   # extent_PAC = compute_extent_PAC(nm,lat,lat_threshold,siconc,siconc_threshold,area,tmask)
   # 
   # f = open(dir_ctrl + 'pickle/PAC/PAC_'+files[:-3] +'.p','wb')
   # #Enregistre chaque extent dans un fichier "extent-1990-1999.p"
   # pickle.dump([extent_PAC],f)
   # f.close()
   # 
   # extent_both = compute_extent_both(nm,lat,lat_threshold,siconc,siconc_threshold,area,tmask)
   # 
   # f = open(dir_ctrl + 'pickle/both/both_'+files[:-3] +'.p','wb')
   # pickle.dump([extent_both],f)
   # f.close()
