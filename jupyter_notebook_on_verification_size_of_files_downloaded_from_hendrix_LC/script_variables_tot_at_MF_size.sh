#!/bin/bash
# Script to run on scylla-ftp1 or scylla-ftp2
###
# ./script_variables_tot_at_MF_size.sh "p1" "p2" "p3"
###
set -x
#
HOST='hendrix.meteo.fr'
#HOST='belenos.meteo.fr'
#
# p1 : First argument = absolute path of the file on hendrix
PATH_H=$1
# p2 : Second argument = where to put the result of the command 
# (ie the file size_files_hendrix_ls) on scylla
PATH_SC=$2
# p3 : Third argument : type of data to select : "*_Amon_*" "*_Omon_*" "*_day_*" 
# "tos_Omon_*"
filter=$3

OUTLIST=${PATH_SC}/'size_files_hendrix_ls'

/usr/bin/ftp  ${HOST} << EOF
ascii
prompt
cd ${PATH_H}
ls ${filter} ${OUTLIST}
bye
EOF

