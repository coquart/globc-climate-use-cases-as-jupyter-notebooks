# GLOBC Climate Use Cases as Jupyter Notebooks

Common Jupyter Notebooks written by GLOBC team

## 26/08/2021 : Creation of the project under Gitlab by L. Coquart : https://gitlab.com
To get it:
git clone https://gitlab.com/coquart/globc-climate-use-cases-as-jupyter-notebooks.git

## Christian Pagé's Notebooks

- https://github.com/cerfacs-globc

- Documentation icclim : https://icclim.readthedocs.io/en/stable/ 

- https://gitlab.com/is-enes-cdi-c4i/notebooks/-/tree/master
